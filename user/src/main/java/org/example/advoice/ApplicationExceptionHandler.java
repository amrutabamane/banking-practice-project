package org.example.advoice;


import jakarta.ejb.ApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.example.exception.UserNotFoundException;

import java.util.HashMap;
import java.util.Map;

@Provider
public class ApplicationExceptionHandler implements ExceptionMapper<UserNotFoundException> {


//    public Map<String, String> handleBusinessException(UserNotFoundException ex) {
//        Map<String, String> errorMap = new HashMap<>();
//        errorMap.put("errorMessage", ex.getMessage());
//        return errorMap;
//    }

    @Override
    public Response toResponse(UserNotFoundException e) {
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("errorMessage", e.getMessage());
        return Response.
                status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMap)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
