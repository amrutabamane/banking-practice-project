package org.example.service;

import org.example.dto.JwtAuthRequestDto;
import org.example.dto.UserDto;
import org.example.entity.UserEntity;
import org.example.exception.UserNotFoundException;

import java.util.List;

public interface UserService {

    UserEntity registerNewUser(UserDto user);

    List<UserEntity> getUserList();

    UserEntity loginUser(JwtAuthRequestDto jwtAuthRequestDto) throws UserNotFoundException;
}
