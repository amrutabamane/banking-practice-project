package org.example.service.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.example.dto.JwtAuthRequestDto;
import org.example.dto.UserDto;
import org.example.entity.UserEntity;
import org.example.exception.UserNotFoundException;
import org.example.repository.UserRepository;
import org.example.service.UserService;

import java.util.List;

@ApplicationScoped
public class UserServiceImpl implements UserService {

    @Inject
    UserRepository userRepository;
    @Override
    public UserEntity registerNewUser(UserDto userDto) {
        UserEntity user = new UserEntity();

        //user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setAbout(userDto.getAbout());

        userRepository.persist(user);


        return user;
    }

    @Override
    public List<UserEntity> getUserList() {
        return userRepository.listAll();
    }

    @Override
    public UserEntity loginUser(JwtAuthRequestDto jwtAuthRequestDto) throws UserNotFoundException {
        UserEntity user = userRepository.findByName(jwtAuthRequestDto.getName());
        if (user!=null){
            return user;
        }
        else {
            throw new UserNotFoundException("user not found with name: "+jwtAuthRequestDto.getName());
        }

    }
}
